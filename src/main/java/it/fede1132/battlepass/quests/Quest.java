package it.fede1132.battlepass.quests;

import com.gmail.fendt873.f32lib.shaded.storage.Yaml;
import it.fede1132.battlepass.BattlePass;
import it.fede1132.battlepass.util.StringUtil;
import org.jetbrains.annotations.NotNull;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Quest {
    public static class Requirement {
        private final int index;
        private final String lang;
        private final Trackable trackable;
        private final String argument;
        private final int required;
        public Requirement(int index, String lang, Trackable requirement, String argument, int required) {
            this.index = index;
            this.lang = lang;
            this.trackable = requirement;
            this.argument = argument;
            this.required = required;
        }

        public int getIndex() {
            return index;
        }

        public String getLang() {
            return lang;
        }

        public Trackable getTrackable() {
            return trackable;
        }

        public String getArgument() {
            return argument;
        }

        public int getRequired() {
            return required;
        }
    }
    public enum QuestType {
        WEEKLY,
        DAILY
    }

    public static List<Quest> parseQuests() {
        BattlePass instance = BattlePass.getInstance();
        Yaml quests = instance.quests;
        List<Quest> map = new ArrayList<>();
        for (String type : quests.getSection("quests").singleLayerKeySet()) {
            try {
                QuestType questType = QuestType.valueOf(type.toUpperCase());
                SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
                for (String key : quests.getSection("quests." + type).singleLayerKeySet()) {
                    String path = "quests." + type + "." + key;
                    String expire = quests.getOrSetDefault(path+".expire", "never");
                    map.add(new Quest(
                            questType,
                            key,
                            quests.getOrSetDefault(path+".name", "&cInvalid name in config!"),
                            quests.getOrSetDefault(path+".requirements", Collections.singletonList("&cInvlid lore in config!")),
                            format.parse(quests.getOrSetDefault(path+".start", "")).getTime(),
                            expire.equals("never") ? 0 : format.parse(expire).getTime(),
                            quests.getOrSetDefault(path+".exp", 0)
                    ));
                }
            } catch (IllegalArgumentException | EnumConstantNotPresentException | ParseException ignored) {System.out.println("SKIPPED");}
        }
        return map;
    }

    private final QuestType type;
    private final String id;
    private final String name;
    private final List<Requirement> requirements = new ArrayList<>();
    private final long start;
    private final long expire;
    private final int exp;
    public Quest(@NotNull QuestType type, @NotNull String id, @NotNull String name, @NotNull List<String> requirements, long start, long expire, int exp) {
        this.type = type;
        this.id = id;
        this.name = name;
        this.start = start;
        this.expire = expire;
        this.exp = exp;

        for (int i=0;i<requirements.size();i++) {
            String requirement = requirements.get(i);
            Pattern pattern = Pattern.compile("^[\\[].\\S*[]]");
            Matcher matcher = pattern.matcher(requirement);
            if (!matcher.find()) continue;
            String group = matcher.group(0);
            String match = group.replace("[", "").replace("]", "");
            String[] req = match.split("\\|");
            if ((req.length == 0 && BattlePass.getInstance().tracksManager.getTrackableById(match) == null)
                    || (req.length > 0 && BattlePass.getInstance().tracksManager.getTrackableById(req[0]) == null)) continue;
            Trackable trackable = BattlePass.getInstance().tracksManager.getTrackableById(req.length > 0 ? req[0] : match);
            String argument = req.length > 0 && trackable.isSupportArgument() ? match.replace(req[0], "").replaceFirst("\\|", "") : null;
            int required = StringUtil.validNum(requirement.replace(group, "").trim())
                    ? Integer.parseInt(requirement.replace(group, "").trim()) : 100;
            if (trackable == null) continue;
            this.requirements.add(new Requirement(i,
                    "trackables." + trackable.getClass().getSimpleName(),
                    trackable,
                    argument,
                    required));
        }
    }

    public QuestType getType() {
        return type;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public List<Requirement> getRequirements() {
        return requirements;
    }

    public long getStart() {
        return start;
    }

    public long getExpire() {
        return expire;
    }

    public int getExp() {
        return exp;
    }
}
