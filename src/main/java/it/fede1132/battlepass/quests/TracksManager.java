package it.fede1132.battlepass.quests;

import java.util.HashMap;

public class TracksManager {
    private final HashMap<String, Trackable> tracks = new HashMap<>();
    public TracksManager register(Trackable trackable) {
        if (trackable.isEnabled())
            tracks.put(trackable.getId(), trackable);
        return this;
    }

    public Trackable getTrackableById(String id) {
        return tracks.getOrDefault(id, null);
    }

    public HashMap<String, Trackable> getTrackables() {
        return tracks;
    }
}
