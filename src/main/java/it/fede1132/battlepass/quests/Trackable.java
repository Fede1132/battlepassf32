package it.fede1132.battlepass.quests;

import it.fede1132.battlepass.BattlePass;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;

import java.util.Arrays;
import java.util.List;

public abstract class Trackable implements Listener {
    private final BattlePass instance;
    private final String id;
    private final boolean enabled;
    private final boolean supportArgument;
    public Trackable(BattlePass instance, String id, boolean enabled, boolean supportArgument) {
        this.instance = instance;
        this.id = id;
        this.enabled = enabled;
        this.supportArgument = supportArgument;
        Bukkit.getPluginManager().registerEvents(this, instance);
    }

    public void setup(){}

    public int get(Player player) { return 0; }
    public int get(Player player, Object object) { return 0; }

    public BattlePass getInstance() {
        return instance;
    }

    public String getId() {
        return id;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public boolean isSupportArgument() {
        return supportArgument;
    }
}
