package it.fede1132.battlepass.quests.trackables;

import it.fede1132.battlepass.BattlePass;
import it.fede1132.battlepass.quests.Trackable;
import it.fede1132.battlepass.util.BattlePassLogic;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Statistic;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerItemBreakEvent;
import org.bukkit.inventory.ItemStack;

public class ItemBroke extends Trackable {
    public ItemBroke(BattlePass instance) {
        super(instance, "items-broke", true, true);
    }
    @Override
    public void setup() {

    }

    @Override
    public int get(Player player) {
        return BattlePassLogic.getTracked(player,this,null);
    }

    @Override
    public int get(Player player, Object obj) {
        return BattlePassLogic.getTracked(player,this,obj);
    }

    @EventHandler
    public void onBroke(PlayerItemBreakEvent event) {
        ItemStack item = event.getBrokenItem();
        Bukkit.getScheduler().runTaskAsynchronously(getInstance(),()->{
            BattlePassLogic.addTracked(event.getPlayer(),this,item.getType(),item.getAmount());
        });
    }
}
