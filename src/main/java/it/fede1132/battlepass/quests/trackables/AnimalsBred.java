package it.fede1132.battlepass.quests.trackables;

import it.fede1132.battlepass.BattlePass;
import it.fede1132.battlepass.quests.Trackable;
import org.bukkit.Statistic;
import org.bukkit.entity.Player;

public class AnimalsBred extends Trackable {

    public AnimalsBred(BattlePass instance) {
        super(instance, "animals-bred", true, false);
    }

    @Override
    public int get(Player player) {
        return player.getStatistic(Statistic.ANIMALS_BRED);
    }
}
