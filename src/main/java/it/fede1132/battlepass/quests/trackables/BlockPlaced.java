package it.fede1132.battlepass.quests.trackables;

import it.fede1132.battlepass.BattlePass;
import it.fede1132.battlepass.quests.Trackable;
import it.fede1132.battlepass.util.BattlePassLogic;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.block.BlockPlaceEvent;

public class BlockPlaced extends Trackable {
    public BlockPlaced() {
        super(BattlePass.getInstance(), "block-placed", true, true);
    }

    @Override
    public int get(Player player) {
        return BattlePassLogic.getTracked(player,this,null);
    }

    @Override
    public int get(Player player, Object object) {
        return BattlePassLogic.getTracked(player,this,object);
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void onBreak(BlockPlaceEvent event) {
        Material type = event.getBlock().getType();
        Bukkit.getScheduler().runTaskAsynchronously(getInstance(), ()->{
            if (event.isCancelled()) return;
            BattlePassLogic.addTracked(event.getPlayer(),this,type,1);
        });
    }
}
