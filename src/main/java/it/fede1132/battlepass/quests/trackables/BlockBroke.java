package it.fede1132.battlepass.quests.trackables;

import it.fede1132.battlepass.BattlePass;
import it.fede1132.battlepass.quests.Trackable;
import it.fede1132.battlepass.util.BattlePassLogic;
import it.fede1132.battlepass.util.ItemUtil;
import it.fede1132.battlepass.util.StringUtil;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Statistic;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.block.BlockBreakEvent;

import java.util.HashMap;
import java.util.Map;

public class BlockBroke extends Trackable {
    public BlockBroke(BattlePass instance) {
        super(instance, "block-broke", true, true);
    }

    @Override
    public void setup() {
    }

    @Override
    public int get(Player player) {
        return BattlePassLogic.getTracked(player,this, null);
    }

    @Override
    public int get(Player player, Object obj) {
        return BattlePassLogic.getTracked(player, this, obj);
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void onBreak(BlockBreakEvent event) {
        Material type = event.getBlock().getType();
        Bukkit.getScheduler().runTaskAsynchronously(getInstance(), ()->{
            if (event.isCancelled()) return;
            BattlePassLogic.addTracked(event.getPlayer(),this,type,1);
        });
    }
}
