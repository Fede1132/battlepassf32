package it.fede1132.battlepass.quests.trackables;

import it.fede1132.battlepass.BattlePass;
import it.fede1132.battlepass.quests.Trackable;
import it.fede1132.battlepass.util.BattlePassLogic;
import org.bukkit.Statistic;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDeathEvent;

public class PlayerKills extends Trackable {

    public PlayerKills(BattlePass instance) {
        super(instance, "player-kills", true, false);
    }

    @Override
    public int get(Player player) {
        return BattlePassLogic.getTracked(player,this,null);
    }

    @EventHandler
    public void onKill(EntityDeathEvent event) {
        if (!event.getEntity().isDead()
                || event.getEntity().getType() != EntityType.PLAYER
                || event.getEntity().getKiller() == null
                || event.getEntity().getKiller().getType() != EntityType.PLAYER) return;
        BattlePassLogic.addTracked((Player) event.getEntity(),this,null,1);
    }
}
