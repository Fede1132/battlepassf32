package it.fede1132.battlepass.quests.trackables;

import it.fede1132.battlepass.BattlePass;
import it.fede1132.battlepass.quests.Trackable;
import me.Cmaaxx.PlayTime.PlayTimeAPI;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class PlayTimes extends Trackable {
    public PlayTimes(BattlePass instance) {
        super(instance, "play-times", Bukkit.getPluginManager().isPluginEnabled("PlayTimes"), true);
    }
    @Override
    public void setup() {

    }

    @Override
    public int get(Player player, Object obj) {
        switch (obj.toString().toLowerCase()) {
            case "years":
                return (int) (double) PlayTimeAPI.getDays(player) / 365;
            case "months":
                return (int) (double) PlayTimeAPI.getDays(player) / 31;
            case "days":
                return PlayTimeAPI.getDays(player);
            case "hours":
                return PlayTimeAPI.getHours(player);
            case "minutes":
                return PlayTimeAPI.getMins(player);
            case "seconds":
                return PlayTimeAPI.getSecs(player);
        }
        return 0;
    }
}
