package it.fede1132.battlepass.quests.trackables;

import it.fede1132.battlepass.BattlePass;
import it.fede1132.battlepass.quests.Trackable;
import it.fede1132.battlepass.util.BattlePassLogic;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Statistic;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.inventory.CraftItemEvent;
import org.bukkit.inventory.ItemStack;

public class ItemCrafted extends Trackable {

    public ItemCrafted(BattlePass instance) {
        super(instance, "item-crafted", true, true);
    }

    @Override
    public int get(Player player) {
        return BattlePassLogic.getTracked(player,this,null);
    }

    @Override
    public int get(Player player, Object obj) {
        return BattlePassLogic.getTracked(player,this,obj);
    }

    @EventHandler(priority = EventPriority.LOW)
    public void onItemCrafted(CraftItemEvent event) {
        ItemStack item = event.getCurrentItem();
        Bukkit.getScheduler().runTaskAsynchronously(getInstance(),()->{
            if (event.isCancelled()) return;
            BattlePassLogic.addTracked((Player) event.getWhoClicked(),this,item.getType(),item.getAmount());
        });
    }
}
