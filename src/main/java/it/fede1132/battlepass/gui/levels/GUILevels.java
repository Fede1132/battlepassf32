package it.fede1132.battlepass.gui.levels;

import com.gmail.fendt873.f32lib.menu.GUI;
import com.gmail.fendt873.f32lib.menu.GUIItem;
import com.gmail.fendt873.f32lib.other.Enchant;
import com.gmail.fendt873.f32lib.other.ItemBuilder;
import com.gmail.fendt873.f32lib.other.Placeholder;
import com.gmail.fendt873.f32lib.shaded.storage.Yaml;
import com.gmail.fendt873.f32lib.shaded.storage.shaded.json.JSONObject;
import it.fede1132.battlepass.BattlePass;
import it.fede1132.battlepass.util.ItemUtil;
import it.fede1132.battlepass.util.BattlePassLogic;
import it.fede1132.battlepass.util.StringUtil;
import org.bukkit.Bukkit;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.jetbrains.annotations.NotNull;

import java.util.*;

public class GUILevels extends GUI {
    private final ItemBuilder LOCKED = ItemUtil.getItemBuilder(BattlePass.getInstance().gui, "levels.items.level-locked");
    private final ItemBuilder CURRENT = ItemUtil.getItemBuilder(BattlePass.getInstance().gui, "levels.items.level-current");
    private final ItemBuilder UNLOCKED = ItemUtil.getItemBuilder(BattlePass.getInstance().gui, "levels.items.level-unlocked");
    private int page;
    private JSONObject data;
    public GUILevels(Player player, int page, JSONObject data) {
        super(BattlePass.getInstance(), player, StringUtil.getGUIString("levels.name", new Placeholder("player", player.getName()), new Placeholder("page", String.valueOf(page))), 36, true);
        this.page = page;
        this.data = data.equals(new JSONObject())
                ? new JSONObject(BattlePass.getInstance().database.getUserData(player.getUniqueId())) : data;
    }
    @Override
    public void body() {
        BattlePass instance = BattlePass.getInstance();
        addItems(instance);
    }

    private void addItems(@NotNull BattlePass instance) {
        items.clear();
        Yaml gui = instance.gui;
        if (gui.getOrDefault("general.fill.enabled", true)) {
            ItemStack item = ItemUtil.getItem(gui, "general.fill.item");
            for (int i = 0; i < getSize(); i++)
                getInv().setItem(i, item);
        }
        Bukkit.getScheduler().runTaskAsynchronously(instance, ()-> {
            Yaml levels = instance.levels;
            String def = levels.getOrDefault("general.max-level", "100");
            int maxLevel = StringUtil.validNum(def) ? Integer.parseInt(def) : 100;
            String style = gui.getOrSetDefault("levels.style", "NORMAL");
            int[] display = new int[(style.equals("NORMAL")?9:45)];
            for (int i=1;i<=display.length;i++) {
                int curr = (page * 9) + i;
                display[(i-1)] = curr > maxLevel ? -1 : curr;
            }
            int playerLevel = BattlePassLogic.getPlayerLevel(getPlayer(), data);
            for (int i=0;i<display.length;i++) {
                if (!getInv().getViewers().contains(getPlayer())) return;
                if (display[i] == -1) continue;
                ItemBuilder item = (playerLevel < display[i] ? LOCKED : (playerLevel == display[i] ? CURRENT : UNLOCKED)).clone();
                item.setPlaceholders(new Placeholder("player_level", String.valueOf(playerLevel)), new Placeholder("level", String.valueOf(display[i])), new Placeholder("player", getPlayer().getName()));
                if (playerLevel >= display[i] && BattlePassLogic.isUnclaimed(getPlayer(), instance.levelsMap.getOrDefault(display[i], null), null, data)) {
                    ItemBuilder unclaim = ItemUtil.getItemBuilder(gui, "levels.items.unclaimed");
                    item.name = StringUtil.color(unclaim.name, new Placeholder("name", item.name));
                    Map<Enchantment, Integer> enchants = unclaim.item.getEnchantments();
                    ItemStack itemUnclaim = unclaim.build();
                    List<String> newLore = itemUnclaim.getItemMeta().getLore();
                    for (String line : newLore) {
                        if (line.equals("{lore}")) {
                            int index = newLore.indexOf(line);
                            newLore.remove(index);
                            newLore.addAll(index, Arrays.asList(item.lore));
                            break;
                        }
                    }
                    item.lore = newLore.toArray(new String[newLore.size()]);
                    item.addFlags(itemUnclaim.getItemMeta().getItemFlags().toArray(new ItemFlag[itemUnclaim.getItemMeta().getItemFlags().size()]));
                    Enchant[] enchs = new Enchant[enchants.size()];
                    for (int j=0;j<enchs.length;j++) {
                        List<Enchantment> bukkitEnchants = new ArrayList<>(enchants.keySet());
                        try {
                            enchs[j] = new Enchant(bukkitEnchants.get(j), enchants.get(j));
                        } catch (NullPointerException e) {
                            enchs[j] = new Enchant(bukkitEnchants.get(j), 1);
                        }
                    }
                    item.addEnchants(enchs);
                }
                int fI = i;
                setItem(new GUIItem(item.build()) {
                    private final int level = display[fI];
                    @Override
                    public boolean click(Player player, InventoryClickEvent event) {
                        player.openInventory(new GUIRewards(player, level, page, data).getInv());
                        return true;
                    }
                }, style.equals("NORMAL")?9 + i:i);
            }
            long start = System.currentTimeMillis();
            if (page != 0)
                setItem(new GUIItem(ItemUtil.getItem(gui, "levels.items.arrow-back")) {
                    @Override
                    public boolean click(Player player, InventoryClickEvent event) {
                        if (System.currentTimeMillis() - start < 150) return false;
                        page--;
                        addItems(instance);
                        return true;
                    }
                }, 18);
            if ((page+1) * 9 + 1 <= maxLevel)
                setItem(new GUIItem(ItemUtil.getItem(gui, "levels.items.arrow-next")) {
                    @Override
                    public boolean click(Player player, InventoryClickEvent event) {
                        if (System.currentTimeMillis() - start < 150) return false;
                        page++;
                        addItems(instance);
                        return true;
                    }
                }, 26);
        });
    }

}
