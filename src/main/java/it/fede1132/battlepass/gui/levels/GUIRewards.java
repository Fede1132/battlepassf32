package it.fede1132.battlepass.gui.levels;

import java.util.*;

import com.gmail.fendt873.f32lib.menu.GUI;
import com.gmail.fendt873.f32lib.menu.GUIItem;
import com.gmail.fendt873.f32lib.other.ItemBuilder;
import com.gmail.fendt873.f32lib.other.Placeholder;
import com.gmail.fendt873.f32lib.shaded.storage.Yaml;
import com.gmail.fendt873.f32lib.shaded.storage.shaded.json.JSONObject;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.command.CommandSender;
import it.fede1132.battlepass.util.levels.Reward;
import org.bukkit.inventory.ItemFlag;
import it.fede1132.battlepass.util.BattlePassLogic;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;
import it.fede1132.battlepass.util.ItemUtil;
import org.bukkit.Bukkit;
import org.bukkit.plugin.Plugin;
import it.fede1132.battlepass.util.StringUtil;
import it.fede1132.battlepass.BattlePass;
import org.bukkit.entity.Player;

public class GUIRewards extends GUI
{
    private final int level;
    private final int oldPage;
    private JSONObject data;
    
    public GUIRewards(final Player player, final int level, final int oldPage, final JSONObject data) {
        super(BattlePass.getInstance(), player, StringUtil.getGUIString("rewards.name", new Placeholder("level", String.valueOf(level))), 54, true);
        this.level = level;
        this.oldPage = oldPage;
        this.data = (data.equals(new JSONObject()) ? new JSONObject(BattlePass.getInstance().database.getUserData(player.getUniqueId())) : data);
    }
    
    @Override
    public void body() {
        this.getInv().clear();
        final BattlePass instance = BattlePass.getInstance();
        final Yaml gui = instance.gui;
        Bukkit.getScheduler().runTaskAsynchronously((Plugin)BattlePass.getInstance(), () -> {
            if (gui.getOrDefault("rewards.items.frame.enabled", true)) {
                List<Integer> slots = new ArrayList<Integer>();
                if (gui.contains("rewards.frame.slot")) {
                    slots.add(StringUtil.validNum(gui.getOrDefault("rewards.items.frame.slot", "0")) ? gui.getInt("rewards.items.frame.slot") : 0);
                }
                else if (gui.contains("rewards.items.frame.slots")) {
                    String[] split = gui.getOrDefault("rewards.items.frame.slots", "0,1").split(",");
                    if (split.length != 0) {
                        for (int i=0;i<split.length;i++) {
                            String num = split[i];
                            if (!(!StringUtil.validNum(num))) {
                                slots.add(Integer.parseInt(num));
                            }
                        }
                    }
                }
                if (slots.size() != 0) {
                    Iterator<Integer> iterator = slots.iterator();
                    while (iterator.hasNext()) {
                        int slot = iterator.next();
                        this.getInv().setItem(slot, ItemUtil.getItem(gui, "rewards.items.frame", new Placeholder[0]));
                    }
                }
            }
            this.setItem(new GUIItem(ItemUtil.getItem(gui, "rewards.items.go-back", new Placeholder[0])) {
                @Override
                public boolean click(final Player player, final InventoryClickEvent event) {
                    player.openInventory(new GUILevels(player, GUIRewards.this.oldPage, GUIRewards.this.data).getInv());
                    return true;
                }
            }, gui.getOrDefault("rewards.items.go-back.slot", 45));
            if (!instance.levelsMap.containsKey(this.level) || instance.levelsMap.get(this.level).getRewards().length == 0) {
                int slot = gui.getOrDefault("rewards.items.empty.slot", 22);
                this.getInv().setItem(slot, ItemUtil.getItem(gui, "rewards.items.empty", new Placeholder[0]));
            }
            else {
                Reward[] rewards = (instance.levelsMap.get(this.level).getRewards());
                for (int j=0;j<rewards.length;j++) {
                    Reward reward = rewards[j];
                    ItemStack item = reward.getDisplay().clone();
                    boolean restricted = (reward.isRestricted() && !BattlePassLogic.hasBattlePass(this.getPlayer(), this.data));
                    if (restricted) {
                        String name = gui.getOrDefault("rewards.items.restricted.name", "{name} &c&l*RESTRICTED*");
                        ItemMeta meta = item.getItemMeta();
                        List<String> lore = meta.getLore();
                        lore.addAll(StringUtil.color(gui.getOrSetDefault("rewards.items.restricted.lore", Arrays.asList("", "&cYou need to buy the Battle Pass to get this item.", "")), new Placeholder[0]));
                        meta.setDisplayName(StringUtil.color(name, new Placeholder("name", meta.getDisplayName())));
                        meta.setLore(lore);
                        item.setItemMeta(meta);
                    }
                    if (BattlePassLogic.isUnclaimed(this.getPlayer(), instance.levelsMap.get(this.level), reward.getId(), this.data) && !restricted && BattlePassLogic.getPlayerLevel(this.getPlayer(), this.data) >= this.level) {
                        ItemBuilder unclaimed = ItemUtil.getItemBuilder(gui, "rewards.items.unclaimed", new Placeholder[0]);
                        Map<Enchantment, Integer> enchants = unclaimed.item.getEnchantments();
                        ItemStack build = unclaimed.build();
                        ItemMeta meta = item.getItemMeta();
                        meta.setDisplayName(StringUtil.color(unclaimed.name, new Placeholder("name", meta.getDisplayName())));
                        List<String> lore = build.getItemMeta().getLore()!=null?build.getItemMeta().getLore():Collections.EMPTY_LIST;
                        if (item.getItemMeta().getLore()!=null) {
                            for (int i=0;i<lore.size();i++) {
                                String line = lore.get(i);
                                if (line.equals("{lore}")) {
                                    lore.remove(i);
                                    lore.addAll(i, item.getItemMeta().getLore());
                                    break;
                                }
                            }
                        }
                        meta.setLore(lore);
                        meta.addItemFlags((ItemFlag[])build.getItemMeta().getItemFlags().toArray(new ItemFlag[build.getItemMeta().getItemFlags().size()]));
                        item.setItemMeta(meta);
                        if (item.getEnchantments().size() > 0 && enchants.size() > 0) {
                            item.addUnsafeEnchantments(enchants);
                        }
                        final Reward fReward = reward;
                        this.addItem(new GUIItem(item) {
                            final Reward reward = fReward;
                            final BattlePass instance = BattlePass.getInstance();
                            @Override
                            public boolean click(final Player player, final InventoryClickEvent event) {
                                if (!BattlePassLogic.isUnclaimed(player, this.instance.levelsMap.get(GUIRewards.this.level), this.reward.getId(), GUIRewards.this.data)) {
                                    return false;
                                }
                                if (this.reward.getType() == Reward.RewardType.ITEM) {
                                    if (player.getInventory().firstEmpty() == -1) {
                                        player.sendMessage(StringUtil.getMessage("rewards.inventory-full", new Placeholder[0]));
                                        return false;
                                    }
                                    player.getInventory().addItem(new ItemStack[] { this.reward.getDisplay() });
                                    GUIRewards.this.getInv().setItem(event.getSlot(), this.reward.getDisplay());
                                    GUIRewards.this.items.remove(event.getSlot());
                                }
                                else if (this.reward.getType() == Reward.RewardType.COMMAND) {
                                    final List<String> commands = (List)this.reward.getValue();
                                    for (String command : commands) {
                                        command = new Placeholder("player", player.getName()).replace(command);
                                        command = new Placeholder("reward", this.reward.getId()).replace(command);
                                        if (command.startsWith("p:")) {
                                            command = command.replace("p:", "");
                                            Bukkit.dispatchCommand((CommandSender)player, command);
                                        }
                                        else if (command.startsWith("c:")) {
                                            command = command.replace("c:", "");
                                            Bukkit.dispatchCommand((CommandSender)Bukkit.getConsoleSender(), command);
                                        }
                                        else {
                                            if (!command.startsWith("m:")) {
                                                continue;
                                            }
                                            com.gmail.fendt873.f32lib.other.StringUtil.sendMessage(getPlayer(), command.replace("m:", ""));
                                        }
                                    }
                                    GUIRewards.this.getInv().setItem(event.getSlot(), this.reward.getDisplay());
                                    GUIRewards.this.items.remove(event.getSlot());
                                }
                                GUIRewards.this.data = BattlePassLogic.setClaimedLevel(player, GUIRewards.this.level, this.reward);
                                return true;
                            }
                        });
                    }
                    else {
                        this.getInv().addItem(new ItemStack[] { item });
                    }
                }
            }
        });
    }
    final String d1="%%__USER__%%";final String d2="%%__USERNAME__%% ";final String d3="%%__RESOURCE__%%";final String d4="%%__VERSION__%%";final String d5="%%__TIMESTAMP__%%";final String d6="%%__FILEHASH__%% ";final String d7="%%__NONCE__%%";
}
