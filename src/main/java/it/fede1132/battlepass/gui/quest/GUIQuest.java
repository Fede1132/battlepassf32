package it.fede1132.battlepass.gui.quest;

import com.gmail.fendt873.f32lib.menu.GUI;
import com.gmail.fendt873.f32lib.menu.GUIItem;
import com.gmail.fendt873.f32lib.other.ItemBuilder;
import com.gmail.fendt873.f32lib.other.Placeholder;
import com.gmail.fendt873.f32lib.shaded.storage.Yaml;
import it.fede1132.battlepass.util.StringUtil;
import org.bukkit.inventory.meta.ItemMeta;
import it.fede1132.battlepass.quests.Trackable;
import org.bukkit.event.inventory.InventoryClickEvent;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.stream.Collectors;
import it.fede1132.battlepass.util.BattlePassLogic;
import java.util.List;
import org.bukkit.inventory.ItemStack;
import java.util.concurrent.atomic.AtomicReference;

import it.fede1132.battlepass.util.ItemUtil;
import org.bukkit.Bukkit;
import it.fede1132.battlepass.BattlePass;
import org.bukkit.entity.Player;
import it.fede1132.battlepass.quests.Quest;

public class GUIQuest extends GUI {
    private final Quest.QuestType type;
    
    public GUIQuest(final Player player, final Quest.QuestType type) {
        super(BattlePass.getInstance(), player, StringUtil.getGUIString("quests." + type.toString().toLowerCase() + ".name", new Placeholder[0]), 54, true);
        this.type = type;
    }
    
    @Override
    public void body() {
        final BattlePass instance = BattlePass.getInstance();
        Bukkit.getScheduler().runTaskAsynchronously(instance, () -> {
            Yaml gui = instance.gui;
            boolean fill = gui.getOrDefault("general.fill.enabled", true);
            ItemStack fillItem = ItemUtil.getItem(gui, "general.fill.item", new Placeholder[0]);
            this.getQuestItems(instance, gui, fill, fillItem);
            int ticks = gui.getOrDefault("quests." + this.type.toString().toLowerCase() + ".update-delay", 20);
            if (ticks > 0) {
                AtomicReference<Integer> task = new AtomicReference<Integer>();
                int taskId = Bukkit.getScheduler().runTaskTimerAsynchronously(instance, () -> {
                    if (this.getPlayer() == null || this.getInv() == null) {
                        Bukkit.getScheduler().cancelTask((int)task.get());
                    } else {
                        this.getQuestItems(instance,gui,fill,fillItem);
                    }
                }, 1L, ticks).getTaskId();
                task.set(taskId);
            }
        });
    }
    
    private void getQuestItems(final BattlePass instance, final Yaml gui, final boolean fill, final ItemStack fillItem) {
        final int[] questSlots = { 10, 11, 12, 13, 14, 15, 16, 19, 20, 21, 22, 23, 24, 25, 28, 29, 30, 31, 32, 33, 34, 37, 34, 38, 39, 41, 42, 43 };
        final List<Quest> quests = instance.questsList.stream()
                .filter(quest -> quest.getType() == this.type)
                .filter(quest -> quest.getStart() < System.currentTimeMillis())
                .filter(quest -> !BattlePassLogic.isQuestClaimed(this.getPlayer(), this.type, quest.getId()))
                .collect(Collectors.toList());
        final ItemBuilder builder = ItemUtil.getItemBuilder(gui, "quests." + this.type.toString().toLowerCase() + ".items.available", new Placeholder[0]);
        final String name = builder.name;
        final String[] lore = builder.lore;
        for (int i = 0; i < Math.min(questSlots.length, quests.size()); ++i) {
            final Quest quest = quests.get(i);
            final List<String> edit = new ArrayList<>();
            boolean completed = true;
            if (quest.getRequirements().size() != 0) {
                for (Quest.Requirement requirement : quest.getRequirements()) {
                    Trackable trackable = requirement.getTrackable();
                    String argument = (requirement.getArgument() == null || requirement.getArgument().equals("")) ? StringUtil.getMessage("global.all") : requirement.getArgument();
                    int required = requirement.getRequired();
                    int current = (requirement.getArgument() == null || !trackable.isSupportArgument()) ? requirement.getTrackable().get(this.getPlayer()) : trackable.get(this.getPlayer(), requirement.getArgument());
                    if (required > current) {
                        completed = false;
                    }
                    final String lang = StringUtil.getMessage(requirement.getLang() + ((current >= required) ? ".complete" : ".in-progress"));
                    Placeholder[] placeholders = new Placeholder[]{new Placeholder("current", String.valueOf(current)), new Placeholder("required", String.valueOf(required)), new Placeholder("type", argument)};
                    if (lang.contains("\n")) {
                        final String[] split = lang.split("\n");
                        for (String string : split) {
                            edit.add(StringUtil.color(string, placeholders));
                        }
                    } else {
                        edit.add(StringUtil.color(lang, placeholders));
                    }
                }
                if (completed || quest.getExpire() == 0L || quest.getExpire() >= System.currentTimeMillis()) {
                    if (!completed) {
                        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyy HH:mm:ss");
                        builder.setPlaceholders(new Placeholder("id", quest.getId()), new Placeholder("name", quest.getName()), new Placeholder("start", format.format(new Date(quest.getStart()))), new Placeholder("end", (quest.getExpire() == 0L) ? instance.messages.getOrDefault("time.lang.never", "never") : format.format(new Date(quest.getExpire()))));
                        ItemStack stack = builder.build();
                        ItemMeta meta = stack.getItemMeta();
                        List<String> metaLore = (List<String>)meta.getLore();
                        for (int j = 0; j < metaLore.size(); ++j) {
                            if (metaLore.get(j).contains("{requirements}")) {
                                metaLore.remove(j);
                                metaLore.addAll(j, edit);
                                break;
                            }
                        }
                        meta.setLore((List<String>)metaLore);
                        stack.setItemMeta(meta);
                        this.setItem(new GUIItem(stack) {
                            @Override
                            public boolean click(final Player player, final InventoryClickEvent event) {
                                return false;
                            }
                        }, questSlots[i]);
                        builder.name = name;
                        builder.lore = lore;
                    } else {
                        this.setItem(new GUIItem(ItemUtil.getItem(gui, "quests." + this.type.toString().toLowerCase() + ".items.complete", new Placeholder[0])) {
                            final String id = quest.getId();
                            @Override
                            public boolean click(final Player player, final InventoryClickEvent event) {
                                GUIQuest.this.getInv().setItem(event.getSlot(), fill ? fillItem : null);
                                BattlePassLogic.setClaimedQuest(player, GUIQuest.this.type, this.id);
                                BattlePassLogic.addPlayerExp(GUIQuest.this.getPlayer(), quest.getExp());
                                return true;
                            }
                        }, questSlots[i]);
                    }
                }
            }
        }
        if (fill) {
            for (int i=0;i<this.getSize();++i) {
                if(getInv().getItem(i)==null) this.getInv().setItem(i, fillItem);
            }
        }
    }
}
