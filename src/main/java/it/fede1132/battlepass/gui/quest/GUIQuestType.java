//
// Decompiled by Procyon v0.5.36
//

package it.fede1132.battlepass.gui.quest;

import com.gmail.fendt873.f32lib.menu.GUI;
import com.gmail.fendt873.f32lib.menu.GUIItem;
import com.gmail.fendt873.f32lib.other.ItemBuilder;
import com.gmail.fendt873.f32lib.other.Placeholder;
import com.gmail.fendt873.f32lib.shaded.storage.Yaml;
import com.gmail.fendt873.f32lib.shaded.storage.shaded.json.JSONObject;
import it.fede1132.battlepass.gui.GUIBattlePass;
import it.fede1132.battlepass.quests.Quest;
import org.bukkit.event.inventory.InventoryClickEvent;
import it.fede1132.battlepass.util.ItemUtil;
import org.bukkit.Bukkit;
import org.bukkit.plugin.Plugin;
import it.fede1132.battlepass.util.StringUtil;
import it.fede1132.battlepass.BattlePass;
import org.bukkit.entity.Player;
public class GUIQuestType extends GUI
{
    private JSONObject data;
    
    public GUIQuestType(final Player player, final JSONObject data) {
        super((Plugin)BattlePass.getInstance(), player, StringUtil.getGUIString("quest-type.name", new Placeholder[0]), 27, true);
        this.data = data;
    }
    
    @Override
    public void body() {
        final BattlePass instance = BattlePass.getInstance();
        final Yaml gui = instance.gui;
        Bukkit.getScheduler().runTaskAsynchronously((Plugin)instance, () -> {
            if (gui.getOrDefault("general.fill.enabled", true)) {
                this.fill(new GUIItem(ItemUtil.getItem(gui, "general.fill.item", new Placeholder[0])) {
                    @Override
                    public boolean click(final Player player, final InventoryClickEvent event) {
                        return false;
                    }
                }, 0, this.getSize() - 1);
            }
            String[] types = new String[] { "weekly", "daily" };
            for (int i=0;i<types.length;i++) {
                String type = types[i];
                int slot;
                if (StringUtil.validNum(gui.getOrDefault("quest-type.items." + type + ".slot", " 0"))) {
                    slot = Integer.parseInt(gui.getOrDefault("quest-type.items." + type + ".slot", " 0"));
                }
                else {
                    slot = 0;
                }
                String fType = type;
                GUIItem item = new GUIItem(ItemBuilder.getItem(getPlayer(),gui,"quest-type.items." + type)) {
                    final String type = fType;
                    @Override
                    public boolean click(final Player player, final InventoryClickEvent event) {
                        player.openInventory(new GUIQuest(player, Quest.QuestType.valueOf(this.type.toUpperCase())).getInv());
                        return true;
                    }
                };
                this.setItem(item, slot);
            }
            if (gui.contains("quest-type.items.back")) {
                int slot;
                if (StringUtil.validNum(gui.getOrDefault("quest-type.items.back.slot", " 0"))) {
                    slot = Integer.parseInt(gui.getOrDefault("quest-type.items.back.slot", " 0"));
                } else {
                    slot = 0;
                }
                setItem(new GUIItem(ItemBuilder.getItem(getPlayer(),gui,"quest-type.items.back")) {
                    @Override
                    public boolean click(Player player, InventoryClickEvent event) {
                        player.openInventory(new GUIBattlePass(player).getInv());
                        return true;
                    }
                }, slot);
            }
        });
    }
}
