package it.fede1132.battlepass.gui;

import com.gmail.fendt873.f32lib.menu.GUI;
import com.gmail.fendt873.f32lib.menu.GUIItem;
import com.gmail.fendt873.f32lib.other.ItemBuilder;
import it.fede1132.battlepass.BattlePass;
import it.fede1132.battlepass.Database;
import it.fede1132.battlepass.quests.Quest;
import it.fede1132.battlepass.util.BattlePassLogic;
import it.fede1132.battlepass.util.StringUtil;
import it.fede1132.battlepass.util.levels.Level;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.meta.ItemMeta;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.List;

public class GUIAdmin extends GUI {
    public GUIAdmin(Player player) {
        super(BattlePass.getInstance(), player, "BattlePass - ADMIN", 27, true);
    }
    @Override
    public void body() {
        BattlePass instance = BattlePass.getInstance();
        Bukkit.getScheduler().runTaskAsynchronously(instance, (()->{
            setItem(new GUIItem(new ItemBuilder(Material.INK_SACK, (short) 10).setDisplayName("&aReload data.").setLore("&7Reload configs, levels, rewards and quests").build()) {
                @Override
                public boolean click(Player player, InventoryClickEvent event) {
                    long start = System.currentTimeMillis();

                    instance.levelsMap.clear();
                    instance.questsList.clear();

                    instance.config.forceReload();
                    instance.gui.forceReload();
                    instance.messages.forceReload();
                    instance.levels.forceReload();
                    instance.levelsMap = Level.parseLevels();
                    instance.quests.forceReload();
                    instance.questsList = Quest.parseQuests();
                    instance.levelsExpMap = BattlePassLogic.reloadLevelExp();

                    instance.database = new Database(instance).init();

                    long end = System.currentTimeMillis();
                    double took = (end - start) / 1000D;
                    NumberFormat format = new DecimalFormat("#0.000");
                    ItemMeta meta = item.getItemMeta();
                    List<String> lore = meta.getLore();
                    if (lore.size() > 1) {
                        lore.remove(1);
                        lore.remove(1);
                        lore.remove(1);
                    }
                    lore.add("");
                    lore.add(StringUtil.color("&aReloaded!"));
                    lore.add(StringUtil.color("&7Took: &f" + format.format(took).replace(",", ".")));
                    meta.setLore(lore);
                    item.setItemMeta(meta);
                    getInv().setItem(13, item);
                    return true;
                }
            }, 13);
        }));
    }
}
