package it.fede1132.battlepass.gui;

import com.gmail.fendt873.f32lib.menu.GUI;
import com.gmail.fendt873.f32lib.menu.GUIItem;
import com.gmail.fendt873.f32lib.other.ItemBuilder;
import com.gmail.fendt873.f32lib.other.Placeholder;
import com.gmail.fendt873.f32lib.shaded.storage.Yaml;
import com.gmail.fendt873.f32lib.shaded.storage.shaded.json.JSONObject;
import it.fede1132.battlepass.BattlePass;
import it.fede1132.battlepass.gui.levels.GUILevels;
import it.fede1132.battlepass.gui.quest.GUIQuest;
import it.fede1132.battlepass.gui.quest.GUIQuestType;
import it.fede1132.battlepass.quests.Quest;
import it.fede1132.battlepass.util.BattlePassLogic;
import it.fede1132.battlepass.util.ItemUtil;
import it.fede1132.battlepass.util.StringUtil;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

import java.util.Set;

public class GUIBattlePass extends GUI {
    public GUIBattlePass(Player player) {
        super(BattlePass.getInstance(), player, StringUtil.getGUIString("main.name", new Placeholder("name", player.getName())), 27, true);
    }
    @Override
    public void body() {
        BattlePass instance = BattlePass.getInstance();
        Yaml gui = instance.gui;
        if (gui.getOrDefault("general.fill.enabled", true))
            fill(new GUIItem(ItemUtil.getItem(gui, "general.fill.item")) {
                @Override
                public boolean click(Player player, InventoryClickEvent event) {
                    return false;
                }
            }, 0, (getSize()-1));
        Bukkit.getScheduler().runTaskAsynchronously(instance, ()-> {
            Set<String> keys = gui.getSection("main.items").singleLayerKeySet();
            for (String key : keys) {
                if (!gui.getOrDefault("main.items." + key + ".enabled", true)) continue;
                if (!validSlot(gui.getOrDefault("main.items." + key + ".slot", "0"))) continue;
                String action = gui.getOrDefault("main.items." + key + ".action", "DISPLAY");
                int slot = gui.getOrDefault("main.items." + key + ".slot", 0);
                JSONObject data = new JSONObject(instance.database.getUserData(getPlayer().getUniqueId()));
                int level = BattlePassLogic.getPlayerLevel(getPlayer(), data);
                ItemStack item = ItemBuilder.getItem(getPlayer(),gui,"main.items."+key+".item",
                        new Placeholder("player", getPlayer().getName()),
                        new Placeholder("player-exp", String.valueOf(BattlePassLogic.getPlayerExp(getPlayer(), data))),
                        new Placeholder("level", String.valueOf(level)),
                        new Placeholder("required-exp", String.valueOf(instance.levelsExpMap.get(level))));
                switch (action) {
                    case "DISPLAY":
                        getInv().setItem(slot, item);
                        break;
                    case "SHOW_LEVELS":
                        setItem(new GUIItem(item) {
                            @Override
                            public boolean click(Player player, InventoryClickEvent event) {
                                player.openInventory(new GUILevels(player, 0, data).getInv());
                                return true;
                            }
                        }, slot);
                        break;
                    case "SHOW_DAILY_QUESTS":
                        setItem(new GUIItem(item) {
                            @Override
                            public boolean click(Player player, InventoryClickEvent event) {
                                player.openInventory(new GUIQuest(player, Quest.QuestType.DAILY).getInv());
                                return true;
                            }
                        }, slot);
                        break;
                    case "SHOW_WEEKLY_QUESTS":
                        setItem(new GUIItem(item) {
                            @Override
                            public boolean click(Player player, InventoryClickEvent event) {
                                player.openInventory(new GUIQuest(player, Quest.QuestType.WEEKLY).getInv());
                                return true;
                            }
                        }, slot);
                        break;
                    case "SHOW_QUESTS":
                        setItem(new GUIItem(item) {
                            @Override
                            public boolean click(Player player, InventoryClickEvent event) {
                                player.openInventory(new GUIQuestType(player, data).getInv());
                                return true;
                            }
                        }, slot);
                        break;
                }
            }
        });
    }

    private boolean validSlot(String s) {
        try {
            int slot = Integer.parseInt(s);
            if (slot >= getSize()) return false;
            return slot >= 0;
        } catch (NumberFormatException e) {
            return false;
        }
    }
}
