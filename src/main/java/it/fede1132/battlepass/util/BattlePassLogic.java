package it.fede1132.battlepass.util;

import com.gmail.fendt873.f32lib.other.Placeholder;
import com.gmail.fendt873.f32lib.shaded.storage.Yaml;
import com.gmail.fendt873.f32lib.shaded.storage.shaded.json.JSONArray;
import com.gmail.fendt873.f32lib.shaded.storage.shaded.json.JSONObject;
import it.fede1132.battlepass.BattlePass;
import it.fede1132.battlepass.Database;
import it.fede1132.battlepass.api.BattlePassPlayerLevelUpEvent;
import it.fede1132.battlepass.quests.Quest;
import it.fede1132.battlepass.quests.Trackable;
import it.fede1132.battlepass.util.levels.Level;
import it.fede1132.battlepass.util.levels.Reward;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import java.util.HashMap;
import java.util.List;

public class BattlePassLogic {
    private static final BattlePass instance = BattlePass.getInstance();
    private static final Database data = instance.database;
    private final static ScriptEngineManager manager = new ScriptEngineManager();
    private final static ScriptEngine js = manager.getEngineByName("js");
    private static final String MATH_PLAIN = "{current_level_exp} * 2";
    private static final String MATH_EXPONENTIAL = "{current_level_exp} * {current_level_exp}";
    private static final String MATH_EXPONENTIAL_2 = "{current_level_exp} * {current_level_exp} * 2";

    // PLAYER LEVEL LOGIC

    public static int getPlayerLevel(Player player, JSONObject arg) {
        JSONObject json = (arg == null ? new JSONObject(data.getUserData(player.getUniqueId())) : arg);
        if (!json.has("level")) return 0;
        return json.getInt("level");
    }

    public static boolean isUnclaimed(Player player, Level level, String id, JSONObject arg) {
        if (level == null) return false;
        JSONObject object = arg == null ? new JSONObject(data.getUserData(player.getUniqueId())) : arg;
        if (!object.has("levelsClaims")) return true;
        JSONObject levelsClaims = object.getJSONObject("levelsClaims");
        if (!levelsClaims.has(String.valueOf(level.getLevel()))) return true;
        List<Object> rewardIds = levelsClaims.getJSONArray(String.valueOf(level.getLevel())).toList();
        if (rewardIds.size() == 0) return true;
        if (id != null) return !rewardIds.contains(id);
        boolean pass = hasBattlePass(player, object);
        for (Reward reward : level.getRewards()) {
            if (reward.isRestricted() && !pass) continue;
            if (!rewardIds.contains(reward.getId())) return true;
        }
        return false;
    }

    public static JSONObject setClaimedLevel(Player player, int level, Reward reward) {
        JSONObject object = new JSONObject(data.getUserData(player.getUniqueId()));
        JSONObject levelsClaims = new JSONObject();
        if (object.has("levelsClaims"))
            levelsClaims = object.getJSONObject("levelsClaims");
        JSONArray array = new JSONArray();
        if (levelsClaims.has(String.valueOf(level)))
            array = levelsClaims.getJSONArray(String.valueOf(level));
        array.put(reward.getId());
        levelsClaims.put(String.valueOf(level), array);
        object.put("levelsClaims", levelsClaims);
        instance.database.setUserData(player.getUniqueId(), object.toString());
        return object;
    }

    // PLAYER EXP LOGIC

    public static int getPlayerExp(Player player, JSONObject arg) {
        JSONObject json = (arg == null ? new JSONObject(data.getUserData(player.getUniqueId())) : arg);
        if (!json.has("exp")) return 0;
        return json.getInt("exp");
    }

    public static void addPlayerExp(Player player, int exp) {
        JSONObject json = new JSONObject(data.getUserData(player.getUniqueId()));
        int currExp = getPlayerExp(player, json) + exp;
        int level = getPlayerLevel(player, json);
        int reqExp = instance.levelsExpMap.get(level);
        while (currExp >= reqExp) {
            Bukkit.getPluginManager().callEvent(new BattlePassPlayerLevelUpEvent(player, level, ++level));
            currExp -= reqExp;
            reqExp = instance.levelsExpMap.get(level);
        }
        json.put("level", level);
        json.put("exp", currExp);
        data.setUserData(player.getUniqueId(), json.toString());
    }

    public static HashMap<Integer, Integer> reloadLevelExp() {
        Yaml levels = BattlePass.getInstance().levels;
        int max = levels.getOrSetDefault("general.max-level", 100);
        String method = levels.getOrSetDefault("general.level-exp.method", "PLAIN");
        String expression = MATH_PLAIN;
        int min = levels.getOrSetDefault("general.level-exp.first-level-exp", 100);
        HashMap<Integer, Integer> exp = new HashMap<Integer, Integer>(){{put(0,min);}};
        if (max > 0) {
            switch (method) {
                case "PLAIN":
                    expression = MATH_PLAIN;
                    break;
                case "EXPONENTIAL":
                    expression = MATH_EXPONENTIAL;
                    break;
                case "EXPONENTIAL_2":
                    expression = MATH_EXPONENTIAL_2;
                    break;
                case "USER-DEFINED":
                    expression = levels.getOrSetDefault("general.level-exp.expression", MATH_PLAIN);
                    break;
            }
            for (int i=1;i<=max;i++) {
                try {
                    Object n = js.eval(new Placeholder("current_level_exp", exp.get(i-1))
                            .replace(new Placeholder("level", String.valueOf(i)).replace(expression)));
                    n = n instanceof Integer?n:Integer.parseInt(String.valueOf(n).split("\\.")[0]);
                    exp.put(i, (int) n);
                } catch (ScriptException e) {
                    e.printStackTrace();
                }
            }
        }
        return exp;
    }

    // QUESTS LOGIC

    public static boolean isQuestClaimed(Player player, Quest.QuestType type, String quest) {
        JSONObject json = new JSONObject(data.getUserData(player.getUniqueId()));
        if (!json.has(type.toString().toLowerCase() + "QuestsClaims")) return false;
        JSONArray array = json.getJSONArray(type.toString().toLowerCase() + "QuestsClaims");
        return array.toList().contains(quest);
    }

    public static void setClaimedQuest(Player player, Quest.QuestType type, String quest) {
        JSONObject json = new JSONObject(data.getUserData(player.getUniqueId()));
        JSONArray array = new JSONArray();
        if (json.has(type.toString().toLowerCase() + "QuestsClaims")) array = json.getJSONArray(type.toString().toLowerCase() + "QuestsClaims");
        array.put(quest);
        json.put(type.toString().toLowerCase() + "QuestsClaims", array);
        data.setUserData(player.getUniqueId(), json.toString());
    }


    // BATTLE PASS LOGIC

    public static boolean hasBattlePass(Player player, JSONObject arg) {
        JSONObject pData = arg == null ? new JSONObject(data.getUserData(player.getUniqueId())) : arg;
        if (!pData.has("BattlePass")) return false;
        return pData.getBoolean("BattlePass");
    }

    public static void setBattlePass(String player, boolean status) {
        JSONObject pData = new JSONObject(data.getUserData(player));
        pData.put("BattlePass", status);
        data.setUserData(player, pData.toString());
    }

    // TRACKABLES LOGIC

    public static int getTracked(Player player, Trackable clazz, Object argument) {
        String s = BattlePassLogic.data.getTrackData(player.getUniqueId(),clazz);
        JSONObject data = new JSONObject(s);
        String var;
        if (argument==null) var="*";
        else if (argument instanceof Material) var=Material.valueOf(String.valueOf(argument)).toString();
        else if (argument instanceof EntityType) var=EntityType.valueOf(String.valueOf(argument)).toString();
        else var=argument.toString();
        return data.has(var)?data.getInt(var):0;
    }

    public static void addTracked(Player player, Trackable clazz, Object argument, int value) {
        JSONObject data = new JSONObject(BattlePassLogic.data.getTrackData(player.getUniqueId(),clazz));
        String var;
        if (argument==null) var="*";
        else if (argument instanceof Material) var=Material.valueOf(String.valueOf(argument)).toString();
        else if (argument instanceof EntityType) var=EntityType.valueOf(String.valueOf(argument)).toString();
        else var=argument.toString();
        int i = (data.has(var)?data.getInt(var):0)+value;
        data.put(var,i);
        if (!var.equals("*")) data.put("*",(data.has("*")?data.getInt("*"):0)+value);
        BattlePassLogic.data.setTrackData(player.getUniqueId(),clazz,data.toString());
    }

}
