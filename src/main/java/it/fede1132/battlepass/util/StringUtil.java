package it.fede1132.battlepass.util;

import com.gmail.fendt873.f32lib.other.Placeholder;
import com.google.common.base.Charsets;
import com.google.common.base.Strings;
import it.fede1132.battlepass.BattlePass;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.Material;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringUtil {
    private static String PREFIX = color(BattlePass.getInstance().messages.getOrDefault("global.prefix", "&9BattlePass>"));
    public static String color(String s, Placeholder... placeholders) {
        s = new Placeholder("prefix", PREFIX).replace(s);
        if (placeholders!=null&&placeholders.length>0) for (Placeholder placeholder : placeholders) s = placeholder.replace(s);
        s = ChatColor.translateAlternateColorCodes('&', s);
        return specialTranslate(s);
    }

    public static List<String> color(List<String> list, Placeholder... placeholders) {
        List<String> newList = new ArrayList<>(list);
        for (int i=0;i<newList.size();i++) {
            String line = newList.get(i);
            newList.remove(i);
            newList.add(i, color(line, placeholders));
        }
        return newList;
    }

    public static String[] coloredListToStringArray(List<String> list, Placeholder... placeholders) {
        String[] arr = new String[list.size()];
        for (int i=0;i<arr.length;i++)
            arr[i] = color(new String(list.get(i).getBytes(), Charsets.UTF_8), placeholders);
        return arr;
    }

    public static String getMessage(String path, Placeholder... placeholders) {
        return color(new String(BattlePass.getInstance().messages.getOrDefault(path, "").getBytes(), Charsets.UTF_8), placeholders);
    }

    public static String getGUIString(String path, Placeholder... placeholders) {
        return color(new String(BattlePass.getInstance().gui.getOrDefault(path, "").getBytes(), Charsets.UTF_8), placeholders);
    }

    public static boolean validNum(String s) {
        try {
            Integer.parseInt(s);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    public static String specialTranslate(String string) {
        Pattern pattern = Pattern.compile(".*(\\[.*?])");
        Matcher matcher = pattern.matcher(string);
        if (!matcher.find()) return string;
        String group = matcher.group(1);
        String[] arguments = group.replace("[", "").replace("]", "").split("\\|");
        if (arguments.length == 0) return string;
        if (arguments[0].equals("bar") && arguments.length > 6) {
            for (int i=1;i<4;i++) if (!validNum(arguments[i])) return string;
            String completed = arguments[5];
            String notCompleted = arguments[6];
            string = string.replace(group, getProgressBar(Integer.parseInt(arguments[1]), Integer.parseInt(arguments[2]), Integer.parseInt(arguments[3]), arguments[4], completed, notCompleted));
        }
        return string;
    }

    private static String getProgressBar(int current, int max, int totalBars, String symbol, String completedColor, String notCompletedColor) {
        float percent = (float) current / max;
        int progressBars = (int) (totalBars * percent);
        return Strings.repeat("" + completedColor + symbol, progressBars)
                + Strings.repeat("" + notCompletedColor + symbol.toCharArray()[0], totalBars - progressBars);
    }

    public static boolean validMaterial(String s) {
        try {
            Material.valueOf(s);
            return true;
        } catch (IllegalArgumentException | EnumConstantNotPresentException e) {
            return false;
        }
    }
}
