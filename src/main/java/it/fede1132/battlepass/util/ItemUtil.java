package it.fede1132.battlepass.util;

import com.gmail.fendt873.f32lib.other.ItemBuilder;
import com.gmail.fendt873.f32lib.other.Placeholder;
import com.gmail.fendt873.f32lib.shaded.storage.Yaml;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;

public class ItemUtil {
    public static ItemStack getItem(Yaml file, String path, Placeholder... placeholders) {
        int amount = validNum(file.getOrDefault(path + ".amount", "")) ? file.getOrDefault(path + ".amount", 1) : 1;
        short data = (short) (validNum(file.getOrDefault(path + ".data", "")) ? file.getOrDefault(path + ".data", 0) : 0);
        boolean isSkull = false;
        String skullData = "";
        String material = file.getOrDefault(path + ".material", "");
        if (material.startsWith("head")) {
            String[] headData = material.split("\\|");
            if (headData.length > 1) {
                isSkull = true;
                for (Placeholder placeholder : placeholders) headData[1] = placeholder.replace(headData[1]);
                skullData = headData[1];
            }
        }
        ItemBuilder builder = new ItemBuilder(validMaterial(material), amount, data);
        builder.setSkull(isSkull);
        builder.setSkullOwner(skullData);
        builder.setPlaceholders(placeholders)
        .setDisplayName(file.getOrDefault(path+".name", ""))
        .setLore(StringUtil.coloredListToStringArray(file.getOrDefault(path+".lore", new ArrayList<>()), placeholders))
        .setUnbreakable(Boolean.parseBoolean(file.getOrDefault(path+".unbreakable", "false")))
        .addFlags(parseFlags(file.getOrDefault(path+".flags", new ArrayList<>())));
        ItemStack item = builder.build();
        for (Object enchant : file.getOrDefault(path+".enchants", new ArrayList<>())) {
            String[] enchData = ((String)enchant).split("\\|");
            if (enchData.length < 2) continue;
            if (!validEnchantment(enchData[0])) continue;
            if (!validNum(enchData[1])) continue;
            boolean safe = Boolean.parseBoolean((enchData.length > 2 ? enchData[2] : "false"));
            if (safe) item.addEnchantment(Enchantment.getByName(enchData[0]), Integer.parseInt(enchData[1]));
            else item.addUnsafeEnchantment(Enchantment.getByName(enchData[0]), Integer.parseInt(enchData[1]));
        }
        return item;
    }

    public static ItemBuilder getItemBuilder(Yaml file, String path, Placeholder... placeholders) {
        int amount = validNum(file.getOrDefault(path + ".amount", "")) ? file.getOrDefault(path + ".amount", 1) : 1;
        short data = (short) (validNum(file.getOrDefault(path + ".data", "")) ? file.getOrDefault(path + ".data", 0) : 0);
        boolean isSkull = false;
        String skullData = "";
        String material = file.getOrDefault(path + ".material", "");
        if (material.startsWith("head")) {
            String[] headData = material.split("\\|");
            if (headData.length > 1) {
                isSkull = true;
                for (Placeholder placeholder : placeholders) headData[1] = placeholder.replace(headData[1]);
                skullData = headData[1];
            }
        }
        ItemBuilder builder = new ItemBuilder(validMaterial(material), amount, data);;
        builder.setSkull(isSkull);
        builder.setSkullOwner(skullData);
        builder.setPlaceholders(placeholders)
                .setDisplayName(file.getOrDefault(path+".name", ""))
                .setLore(StringUtil.coloredListToStringArray(file.getOrDefault(path+".lore", new ArrayList<>())))
                .setUnbreakable(Boolean.parseBoolean(file.getOrDefault(path+".unbreakable", "false")))
                .addFlags(parseFlags(file.getOrDefault(path+".flags", new ArrayList<>())));
        for (Object enchant : file.getOrDefault(path+".enchants", new ArrayList<>())) {
            String[] enchData = ((String)enchant).split("\\|");
            if (enchData.length < 2) continue;
            if (!validEnchantment(enchData[0])) continue;
            if (!validNum(enchData[1])) continue;
            boolean safe = Boolean.parseBoolean((enchData.length > 2 ? enchData[2] : "false"));
            if (safe) builder.item.addEnchantment(Enchantment.getByName(enchData[0]), Integer.parseInt(enchData[1]));
            else builder.item.addUnsafeEnchantment(Enchantment.getByName(enchData[0]), Integer.parseInt(enchData[1]));
        }
        return builder;
    }

    private static Material validMaterial(String s) {
        return Material.getMaterial(s) == null ?  Material.STONE : Material.getMaterial(s);
    }

    private static Material getSkullMaterial() {
        try {
            return Material.getMaterial(397);
        } catch (NoSuchMethodError e) {
            return Material.valueOf("LEGACY_PLAYER_HEAD");
        }
    }

    private static boolean validNum(String num) {
        try {
            Double.parseDouble(num);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    private static boolean validEnchantment(String s) {
        return Enchantment.getByName(s) != null;
    }

    private static ItemFlag[] parseFlags(List<String> flags) {
        List<ItemFlag> newList = new ArrayList<>();
        for (String flag : flags) {
            if (!validFlag(flag)) continue;
            newList.add(ItemFlag.valueOf(flag));
        }
        ItemFlag[] arr = new ItemFlag[newList.size()];
        return newList.toArray(arr);
    }

    private static boolean validFlag(String s) {
        try {
            ItemFlag.valueOf(s.toUpperCase());
            return true;
        } catch (EnumConstantNotPresentException e) {
            return false;
        }
    }
}
