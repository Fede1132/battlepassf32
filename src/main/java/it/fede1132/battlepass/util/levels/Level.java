package it.fede1132.battlepass.util.levels;

import com.gmail.fendt873.f32lib.shaded.storage.Yaml;
import it.fede1132.battlepass.BattlePass;
import it.fede1132.battlepass.util.StringUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Level {
    private final int level;
    private final Reward[] rewards;
    public static HashMap<Integer, Level> parseLevels() {
        HashMap<Integer, Level> levels = new HashMap<>();
        Yaml file = BattlePass.getInstance().levels;
        for (String key : file.getSection("levels").singleLayerKeySet()) {
            if (!file.contains("levels."+key+".rewards")) continue;
            if (!StringUtil.validNum(key)) continue;
            List<Reward> rewards = new ArrayList<>();
            for (String reward : file.getSection("levels."+key+".rewards").singleLayerKeySet()) {
                rewards.add(Reward.parseReward(file, "levels."+key+".rewards."+reward));
            }
            Reward[] arr = new Reward[rewards.size()];
            int level = Integer.parseInt(key);
            levels.put(level, new Level(level, rewards.toArray(arr)));
        }
        return levels;
    }
    public Level(int level, Reward[] rewards) {
        this.level = level;
        this.rewards = rewards;
    }

    public int getLevel() {
        return level;
    }

    public Reward[] getRewards() {
        return rewards;
    }
}
