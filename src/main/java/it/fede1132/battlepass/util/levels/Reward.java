package it.fede1132.battlepass.util.levels;

import com.gmail.fendt873.f32lib.shaded.storage.Yaml;
import it.fede1132.battlepass.util.ItemUtil;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;

public class Reward {
    public enum RewardType {
        ITEM,
        COMMAND;
    }

    public static Reward parseReward(Yaml file, String section) {
        try {
            RewardType type = RewardType.valueOf(file.getOrDefault(section+".type", "unknown"));
            String[] split = section.split("\\.");
            ItemStack display = null;
            Object value = null;
            switch (type) {
                case ITEM: {
                    display = ItemUtil.getItem(file, section);
                    break;
                }
                case COMMAND: {
                    display = ItemUtil.getItem(file, section);
                    value = file.getOrDefault(section+".commands", new ArrayList<>());
                    break;
                }
            }
            if (display == null) return null;
            return new Reward(split[(split.length-1)], type, Boolean.parseBoolean(file.getOrDefault(section+".restricted", "false")), display, value);
        } catch (IllegalArgumentException | EnumConstantNotPresentException e) {
            return null;
        }
    }

    private final String id;
    private final RewardType type;
    private final boolean restricted;
    private final ItemStack display;
    private final Object value;
    public Reward(String id, RewardType type, boolean restricted, ItemStack display, Object value) {
        this.id = id;
        this.type = type;
        this.restricted = restricted;
        this.display = display;
        this.value = value;
    }

    public String getId() {
        return id;
    }

    public RewardType getType() {
        return type;
    }

    public boolean isRestricted() {
        return restricted;
    }

    public ItemStack getDisplay() {
        return display;
    }

    public Object getValue() {
        return value;
    }
}
