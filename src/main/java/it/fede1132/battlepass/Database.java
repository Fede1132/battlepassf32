package it.fede1132.battlepass;

import com.zaxxer.hikari.HikariDataSource;
import it.fede1132.battlepass.quests.Trackable;
import it.fede1132.battlepass.quests.trackables.AnimalsBred;
import org.bukkit.entity.Player;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;

public class Database {
    private final BattlePass instance;
    // Pool
    HikariDataSource pool;
    // MySQL
    private String host;
    private String database;
    private String user;
    private String password;
    // Miscellaneous
    private int size = 15;
    private String userTable;
    private String trackTable;
    // Type
    private boolean local = true;

    public Database(BattlePass instance) {
        this.instance = instance;
        if (instance.config.getOrSetDefault("database.type", "SQLite").toLowerCase().equals("mysql")) {
            local=false;
            host = instance.config.getOrSetDefault("database.credentials.host", "127.0.0.1");
            database = instance.config.getOrSetDefault("database.credentials.database", "127.0.0.1");
            user = instance.config.getOrSetDefault("database.credentials.user", "127.0.0.1");
            password = instance.config.getOrSetDefault("database.credentials.pass", "127.0.0.1");
        }
        size = instance.config.getOrSetDefault("database.credentials.pass.pool-size", 15);
        userTable = instance.config.getOrSetDefault("database.credentials.table-name-users", "battlepass_users");
        trackTable = instance.config.getOrSetDefault("database.credentials.table-name-tracks", "battlepass_users");
    }

    public Database init() {
        try (Connection connection = getConnection()) {
            connection.prepareStatement(getTableUsers()).executeUpdate();
            connection.prepareStatement(getTrackTable()).executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return this;
    }

    public void checkPlayer(Player player) {
        try (Connection connection = getConnection()) {
            PreparedStatement check = connection.prepareStatement("SELECT * FROM " + userTable + " WHERE uuid = ?");
            check.setString(1,player.getUniqueId().toString());
            ResultSet rs = check.executeQuery();
            boolean next = rs.next();
            if (next&&!player.getName().equals(rs.getString("name"))) {
                PreparedStatement ps = connection.prepareStatement("UPDATE " + userTable + " SET name = ? WHERE uuid = ?");
                ps.setString(1,player.getName());
                ps.setString(2,player.getUniqueId().toString());
                ps.executeUpdate();
            } else if (!next) {
                PreparedStatement ps = connection.prepareStatement("INSERT INTO " + userTable + " (uuid, name, data) VALUES (?, ?, ?)");
                ps.setString(1, player.getUniqueId().toString());
                ps.setString(2, player.getName());
                ps.setString(3, "{}");
                ps.executeUpdate();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public boolean exists(Object identifier, Connection connection) {
        try {
            PreparedStatement check = connection.prepareStatement("SELECT * FROM " + userTable + " WHERE " + (identifier instanceof UUID ? "uuid" : "name") + " = ?");
            check.setString(1,identifier.toString());
            ResultSet rs = check.executeQuery();
            return rs.next();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public String getUserData(Object identifier, Connection connection) {
        try {
            PreparedStatement check = connection.prepareStatement("SELECT * FROM " + userTable + " WHERE " + (identifier instanceof UUID ? "uuid" : "name") + " = ?");
            check.setString(1,(identifier instanceof UUID ? identifier.toString() : String.valueOf(identifier)));
            ResultSet rs = check.executeQuery();
            if (rs.next()) return rs.getString("data");
            return "{}";
        } catch (SQLException | NullPointerException e) {
            e.printStackTrace();
        }
        return "";
    }

    public String getUserData(Object identifier) {
        try (Connection connection = getConnection()) {
            return getUserData(identifier,connection);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return "";
    }

    public void setUserData(Object identifier, String data) {
        try (Connection connection = getConnection()) {
            PreparedStatement update = connection.prepareStatement("UPDATE " + userTable + " SET data = ? WHERE " + (identifier instanceof UUID ? "uuid" : "name") + " = ?");
            update.setString(1,data);
            update.setString(2,(identifier instanceof UUID ? identifier.toString() : String.valueOf(identifier)));
            update.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public String getTrackData(UUID uuid, Trackable track, Connection connection) {
        try {
            PreparedStatement check = connection.prepareStatement("SELECT * FROM " + trackTable + " WHERE uuid = ?");
            check.setString(1,uuid.toString());
            ResultSet rs = check.executeQuery();
            if (rs.next()) return rs.getString(track.getClass().getSimpleName());
            return "{}";
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return "";
    }

    public String getTrackData(UUID uuid, Trackable trackable) {
        try (Connection connection = getConnection()) {
            return getTrackData(uuid,trackable,connection);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return "";
    }

    public void setTrackData(UUID uuid, Trackable track, String data) {
        try (Connection connection = getConnection()) {
            boolean exists = !getTrackData(uuid,track,connection).equals("{}");
            String trackName = track.getClass().getSimpleName();
            PreparedStatement update = connection.prepareStatement(
                    exists?"UPDATE " + trackTable + " SET " + trackName + " = ? WHERE uuid = ?":
                    "INSERT INTO " + trackTable + " ("+ trackName + ", uuid) VALUES (?, ?)");
            update.setString(1,data);
            update.setString(2,uuid.toString());
            update.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public Connection getConnection() {
        try {
            if (pool==null) {
                pool = new HikariDataSource();
                File file = new File(instance.getDataFolder(),"database.db");
                if (local&&!file.exists()) file.createNewFile();
                pool.setDriverClassName(local?"org.sqlite.JDBC":"com.mysql.jdbc.Driver");
                pool.setJdbcUrl("jdbc:" + (local?"sqlite:"+file:"mysql://"+host+"/"+database));
                if (!local) {
                    pool.setUsername(user);
                    pool.setPassword(password);
                }
                pool.setMaximumPoolSize(size);
                pool.addDataSourceProperty("useSSL", false);
            }
            return pool.getConnection();
        } catch (SQLException | IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private String getTableUsers() {
        return "CREATE TABLE IF NOT EXISTS " + userTable + " (`uuid` char(36), `name` varchar(16), `data` TEXT NOT NULL, PRIMARY KEY (`uuid`, `name`));";
    }

    private String getTrackTable() {
        StringBuilder query = new StringBuilder("CREATE TABLE IF NOT EXISTS " + trackTable + " ( `uuid` char(36) NOT NULL, ");
        for (Trackable track : instance.tracksManager.getTrackables().values()) {
            query.append("`").append(track.getClass().getSimpleName()).append("`").append(" TEXT, ");
        }
        return query.append("PRIMARY KEY (`uuid`));").toString();
    }
}
