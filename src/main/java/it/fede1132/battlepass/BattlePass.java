package it.fede1132.battlepass;

import com.gmail.fendt873.f32lib.F32Lib;
import com.gmail.fendt873.f32lib.commands.CmdArgs;
import com.gmail.fendt873.f32lib.commands.Command;
import com.gmail.fendt873.f32lib.shaded.storage.LightningBuilder;
import com.gmail.fendt873.f32lib.shaded.storage.Yaml;
import com.gmail.fendt873.f32lib.shaded.storage.internal.settings.ConfigSettings;
import com.gmail.fendt873.f32lib.shaded.storage.internal.settings.ReloadSettings;
import it.fede1132.battlepass.cmds.Args;
import it.fede1132.battlepass.quests.Quest;
import it.fede1132.battlepass.quests.TracksManager;
import it.fede1132.battlepass.quests.trackables.*;
import it.fede1132.battlepass.util.BattlePassLogic;
import it.fede1132.battlepass.util.levels.Level;
import org.bstats.bukkit.Metrics;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.util.HashMap;
import java.util.List;

public final class BattlePass extends JavaPlugin implements Listener {
    private static BattlePass instance;
    private static F32Lib core;
    // configs
    public Yaml config;
    public Yaml gui;
    public Yaml levels;
    public Yaml quests;
    public Yaml messages;
    // levels
    public HashMap<Integer, Level> levelsMap = new HashMap<>();
    public HashMap<Integer, Integer> levelsExpMap = new HashMap<>();
    // Quests
    public TracksManager tracksManager;
    public List<Quest> questsList;
    // Data
    public Database database;
    @Override
    public void onEnable() {
        // instance setting
        instance = this;
        config = LightningBuilder.fromFile(new File(getDataFolder(), "config")).addInputStream(getResource("config.yml")).setConfigSettings(ConfigSettings.PRESERVE_COMMENTS).setReloadSettings(ReloadSettings.MANUALLY).createYaml();
        // Database loading
        tracksManager = new TracksManager()
                .register(new BlockBroke(this))
                .register(new ItemBroke(this))
                .register(new ItemCrafted(this))
                .register(new MobKill(this))
                .register(new PlayerKills(this))
                .register(new PlayTimes(this));
        core = new F32Lib(this);
        database = new Database(this).init();
        new Metrics(this, 7826);
        // files
        gui = LightningBuilder.fromFile(new File(getDataFolder(), "gui")).addInputStream(getResource("gui.yml")).setConfigSettings(ConfigSettings.PRESERVE_COMMENTS).setReloadSettings(ReloadSettings.MANUALLY).createYaml();
        levels = LightningBuilder.fromFile(new File(getDataFolder(), "levels")).addInputStream(getResource("levels.yml")).setConfigSettings(ConfigSettings.PRESERVE_COMMENTS).setReloadSettings(ReloadSettings.MANUALLY).createYaml();
        quests = LightningBuilder.fromFile(new File(getDataFolder(), "quests")).addInputStream(getResource("quests.yml")).setConfigSettings(ConfigSettings.PRESERVE_COMMENTS).setReloadSettings(ReloadSettings.MANUALLY).createYaml();
        messages = LightningBuilder.fromFile(new File(getDataFolder(), "messages")).addInputStream(getResource("messages.yml")).setConfigSettings(ConfigSettings.PRESERVE_COMMENTS).setReloadSettings(ReloadSettings.MANUALLY).createYaml();
        core.reloadHeads();
        getLogger().info("(!) Files loaded.");
        //getCommand("battlepass").setExecutor(new CmdBattlePass());
        new Command(this, "battlepass", new Command.ExecuteEvent() {
            @Override
            public boolean onExecute(CmdArgs args) {
                if (!args.getSender().isOp()) {
                    new Args().openArg(args);
                    return false;
                }
                return true;
            }
        }).addAll(new Args());
        getLogger().info("(!) Command loaded.");
        // levels, trackables and quest loading
        levelsMap.putAll(Level.parseLevels());
        levelsExpMap = BattlePassLogic.reloadLevelExp();
        getLogger().info("(!) Rewards loaded.");
        questsList = Quest.parseQuests();
        // Player Join Event
        Bukkit.getPluginManager().registerEvents(this,this);
        getLogger().info("(!) Quests loaded.");
        getLogger().info("(!) Finished plugin loading.");
    }

    @Override
    public void onDisable() { }

    public static BattlePass getInstance() {
        return instance;
    }

    @EventHandler(priority = EventPriority.HIGH)
    public void onPlayerJoin(PlayerJoinEvent event) {
        database.checkPlayer(event.getPlayer());
    }
}
