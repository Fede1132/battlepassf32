package it.fede1132.battlepass.cmds;

import com.gmail.fendt873.f32lib.commands.CmdArgs;
import com.gmail.fendt873.f32lib.commands.SubArg;
import it.fede1132.battlepass.BattlePass;
import it.fede1132.battlepass.gui.GUIAdmin;
import it.fede1132.battlepass.gui.GUIBattlePass;
import it.fede1132.battlepass.util.BattlePassLogic;
import org.bukkit.entity.Player;

public class Args {
    @SubArg(
            arg = "open",
            description = "Open the main gui.",
            usage = "open",
            requiresPlayer = true,
            reqArgs = 0,
            permission = "open"
    )
    public void openArg(CmdArgs args) {
        Player player = (Player) args.getSender();
        player.openInventory(new GUIBattlePass(player).getInv());
    }

    @SubArg(
            arg = "admin",
            description = "Open the admin gui",
            usage = "admin",
            reqArgs = 0,
            permission = "admin"
    )
    public void adminArg(CmdArgs cmdArgs) {
        if (cmdArgs.getSender()instanceof Player && cmdArgs.getArgs().length==1) {
            Player player = (Player) cmdArgs.getSender();
            player.openInventory(new GUIAdmin(player).getInv());
            return;
        }
        String[] args = cmdArgs.getArgs();
        String label = cmdArgs.getLabel().split(" ")[0];
        // /pass admin givepass Fede1132
        //        0       1       2
        if (args.length<3) {
            cmdArgs.getSender().sendMessage(new String[]{"Incorrect usage, available commands:",
                    label + " admin givepass <player> :: Give battlepass to specified player",
                    label + " admin removepass <player> :: Removes pass from a player"});
            return;
        }
        switch (args[1].toLowerCase()) {
            case "givepass": {
                String name = args[2];
                System.out.println(name);
                BattlePassLogic.setBattlePass(name,true);
                cmdArgs.getSender().sendMessage("Successfully gave BattlePass to " + name);
                break;
            }
            case "removepass": {
                String name = args[2];
                BattlePassLogic.setBattlePass(name,false);
                cmdArgs.getSender().sendMessage("Successfully removed BattlePass from " + name);
                break;
            }
        }
    }
}
