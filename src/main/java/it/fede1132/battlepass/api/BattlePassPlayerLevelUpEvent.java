package it.fede1132.battlepass.api;

import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class BattlePassPlayerLevelUpEvent extends Event {
    private static HandlerList handlerList = new HandlerList();
    private final Player player;
    private final int oldLevel;
    private final int newLevel;
    public BattlePassPlayerLevelUpEvent(Player player, int oldLevel, int newLevel) {
        this.player = player;
        this.oldLevel = oldLevel;
        this.newLevel = newLevel;
    }

    @Override
    public HandlerList getHandlers() {
        return handlerList;
    }

    public static HandlerList getHandlerList() { return handlerList; }

    @Override
    public String getEventName() {
        return "BattlePassPlayerLevelUpEvent";
    }

    public Player getPlayer() {
        return player;
    }

    public int getOldLevel() {
        return oldLevel;
    }

    public int getNewLevel() {
        return newLevel;
    }
}
